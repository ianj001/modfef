# MOD FE FILES
# This program will modify the files output by F-Engrave.
# It will give you the option to do the following;
#           Change comment characters
#           Change the file extension
#           Concatenate the v-clean file
#           Concatenate the clean file
#           Save the default configuration
#           Save the user configuration
#           Load a previous user configuration

# It is written in Python
# Author: Ian Jobson
# Email: ianj001@gmail.com
# Program Name: MODfef.py
# Version: 2.1.2


# General imports
import configparser
import os
import shutil
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import *
from ToolTip import ToolTip
global pizza
global version 
version = '2.1.2'
# Toggle the checkbox
def toggle_check():
    checkb.toggle()
    check_box()
    return()
# If the INI file doesn't exist let's write one
def check_configfile():
    if not os.path.exists('MODfef.ini'):
        config = configparser.ConfigParser()
        config['Default'] = {'extension': 'gcode', 'commentchar': ';', 'toolchange': 'M0 ==== TOOL CHANGE ====', 'concatenate': 'Yes'}
        config['User'] = {'extension': 'gcode', 'commentchar': ';', 'toolchange': 'M0', 'concatenate': 'Yes'}
        with open('MODfef.ini', 'w') as configfile:
            config.write(configfile)
    return()
# Read the USER saved configuration
def config_read_user():
    extensioncr = config_read('extension', 'User')
    commentcharcr = config_read('commentchar', 'User')
    toolchangecr = config_read('toolchange', 'User')
    concatenatecr = config_read('concatenate', 'User')
    print(concatenatecr)
    gyro.config(text=extensioncr)
    pizza.config(text=commentcharcr)
    cod.config(text=toolchangecr)
    if concatenatecr == 1:
        checkb.select()
        print("its true we are here")
    else:
        checkb.deselect()
    return()
# Read the configuration INI file
def config_read(param, section):
    config = configparser.ConfigParser()
    config.sections()
    config.read('MODfef.ini')
    if param == "extension":
        param = config[section]['extension']
    elif param == "commentchar":
        param = config[section]['commentchar']
    elif param == "toolchange":
        param = config[section]['toolchange']
    elif param == "concatenate":
        param = config.getboolean(section,'concatenate')
    return(param)

# Save the Default config file
def config_default_save():
    extension_label = gyro.cget("text")
    comment_label = pizza.cget("text")
    toolchange_label = cod.cget("text")
    conc_yesno =conc_file.get()
    config_save('extension', extension_label, 'Default')
    config_save('commentchar', comment_label, 'Default')
    config_save('toolchange', toolchange_label, 'Default')
    config_save('concatenate', conc_yesno, 'Default')
    output.insert(END, "The DEFAULT settings were updated" + '\n')
    return()
# Save config file
def config_save(param, param2, param3):
    config = configparser.ConfigParser()
    config.sections()
    config.read('MODfef.INI')
    if param == "extension":
        config[param3]['extension'] = param2
        output.insert(END, "The EXTENSION was updated" + '\n')
    elif param == "commentchar":
        config[param3]['commentchar'] = param2
        output.insert(END, "The CHARACTER used for comments was updated" + '\n')
    elif param == "toolchange":
        config[param3]['toolchange'] = param2
        output.insert(END, "The TOOL CHANGE gcode line was updated" + '\n')
    elif param == "concatenate":
        config[param3]['concatenate'] = param2
        output.insert(END, "The CONCATENATION check box was updated" + '\n')
    with open('MODfef.INI', 'w') as configfile:    # save
        config.write(configfile)
    return()


# Select a file
def select_file(param):
    selected_file = 'dummyGcodeFile.txt'
    window.fileName = filedialog.askopenfilename( filetypes = ( ("ngc files", "*.ngc"), ( "all files", "*.*")))
    if param == "first_file":
        first_file.config(text=window.fileName)
        #startb.configure(state='enabled')
        instruction1_label.config(fg="gray")
        output.insert(END, "The MAIN Gcode file was selected" + '\n')
    elif param == "vclean_file":
        vclean_file.config(text=window.fileName)
        instruction2_label.config(fg="gray")
        output.insert(END, "The VCLEAN file was selected" + '\n')
    elif param == "clean_file":
        clean_file.config(text=window.fileName)
        instruction3_label.config(fg="gray")
        output.insert(END, "The CLEAN file was selected" + '\n')
    elif param == "begin_file":
        begin_file.config(text=window.fileName)
        instruction8_label.config(fg="gray")
        output.insert(END, "The START file was selected" + '\n')
    elif param == "end_file":
        end_file.config(text=window.fileName)
        instruction9_label.config(fg="gray")
        output.insert(END, "The END file was selected" + '\n')
    return()


# Modify main file
def modify_main(gcode_file, extension, commentchar):
    with open(gcode_file, 'r') as fr:
        splitname = os.path.splitext(fr.name)
        basename = splitname[0]
        newname = basename + '.' + extension
        with open(newname, 'w') as fw:
            for line in fr:
                message = line
                #print(message) 
                message_start = message[0]
                if message_start == '(':
                    message = message.replace(message_start, commentchar + message_start)
                    #print(message)
                fw.write(message)
        
    return(newname)
# Add tool change line
def add_toolchange(mainfile, line):
    with open(mainfile, 'a') as fw2:
        pretool_change = "M400\nG0 Z40 F300\nG0 X0 Y0 F500\nM400\n"
        message = commentchar + ' #####Tool change line added below here\n' + pretool_change + line +'\n'
# Need to add some additional lines of code for the tool change to work
# The user input can only be a one command, not multiple commands on one line
# Will add M400\n G0 Z40 F300\n M400\n G0 X0 Y0 F500\n then the users line (typically M0)
        fw2.write(message)
        output.insert(END, "Added the toolchange line to the file...." + '\n')
    #writing tool change line happens here
    return()
# Concatenate file
def concatenate_file(mainfile, catfile, commentchar):
    with open(catfile, 'r') as fr2:
        with open(mainfile, 'a') as fw2:
            message = commentchar + ' Concatonated file ' + catfile + ' below here\n'
            fw2.write(message)
            for line in fr2:
               message = line
               message_start = message[0]
               if message_start == '(':
                  message = message.replace(message_start, commentchar + message_start)
               fw2.write(message)
            message = '\n'
            fw2.write(message)
    return()
def prepend_file(mainfile, prependfile, commentchar):
    # lets make a copy of the prependfile
    tempfile = 'x14r3vb653'
    shutil.copy(prependfile,tempfile)
    with open(mainfile, 'r') as fr2:
        with open(prependfile, 'a') as fw2:
            message = '\n' + commentchar + ' Prepended file above here\n'
            fw2.write(message)
            for line in fr2:
               message = line
               message_start = message[0]
               if message_start == '(':
                  message = message.replace(message_start, commentchar + message_start)
               fw2.write(message)
            message = '\n'
            fw2.write(message)
    os.remove(mainfile)
    os.rename(prependfile,mainfile)
    os.rename(tempfile,prependfile)
    return()
# Input dialog box
def user_input(thing):
    user_response = simpledialog.askstring(title="Changing " + thing, prompt="Enter the value for " + thing + ":")
    if thing == "extension":
        gyro.config(text=user_response)
        config_save('extension', user_response, 'User')
        instruction4_label.config(fg="gray")
    elif thing == "commentchar":
        pizza.config(text=user_response)
        config_save('commentchar', user_response, 'User')
        instruction5_label.config(fg="gray")
    elif thing == "toolchange":
        cod.config(text=user_response)
        config_save('toolchange', user_response, 'User')
        instruction6_label.config(fg="gray")
    elif thing == "concatenate":
        pizza.config(text=user_response)
        config_save('concatenate', user_response, 'User')
    return()
# Record the check box result
def check_box():
    if conc_file.get() == 1: 
        # print('ON')
        config_save('concatenate', "Yes", 'User')
    else:
        # print('OFF')
        config_save('concatenate', "No", 'User') 
    #config_save('concatenate', on_off, 'User')
    instruction7_label.config(fg="gray")
    return()
def popup(msg):
    popup = Tk()
    popup.title("Error!")
    popup.geometry("350x100")
    label = Label(popup, text=msg)
    label.pack(side="top", fill="x", pady=10)
    B1 = Button(popup, text="OK", width=9, command = popup.destroy)
    B1.pack()
    popup.mainloop()
    return()
# Start Button hit
def start_button():
    file_main = first_file.cget("text")
    file_vclean = vclean_file.cget("text")
    file_clean = clean_file.cget("text")
    file_start = begin_file.cget("text")
    file_end = end_file.cget("text")
    extension_label = gyro.cget("text")
    comment_label = pizza.cget("text")
    toolchange_label = cod.cget("text")
    conc_yesno =conc_file.get()
#if the file_main filename is still No MAIN file selected then pop up a reminder and leave.
    if file_main == 'No MAIN file selected':
        popup("No MAIN file selected.")
        return()
    output.insert(END, "File updates started...." + '\n')
    output.insert(END, "Processing the MAIN file...." + '\n')
    newname = modify_main(file_main, extension_label, comment_label)
    if conc_yesno == 1:
        if file_vclean != 'No V-CLEAN file selected':
            concatenate_file(newname, file_vclean, commentchar)
            output.insert(END, "Processing V-CLEAN file...")
        else:
            output.insert(END, "No V-CLEAN file to process...")
        if file_clean != 'No CLEAN file selected':
            add_toolchange(newname, toolchange_label)
            concatenate_file(newname, file_clean, commentchar)
            output.insert(END, "Processing CLEAN file...")
        else:
            output.insert(END, "No CLEAN file to process...")
        if file_start != 'No START file selected':
            temp_file_start = "x1f5d3g4.ngc"
            shutil.copy(file_start, temp_file_start)
            modify_main(temp_file_start, extension_label, comment_label)
           
            new_file_start = os.path.splitext(temp_file_start)
            basename = new_file_start[0]
            file_start2 = basename + '.' + extension_label
            prepend_file(newname, file_start2, commentchar)
            os.remove(temp_file_start)
            os.remove(file_start2)
            #os.rename("tempabc123",file_start)
            output.insert(END, "Processing START file...")
        else:
            output.insert(END, "No START file to process...")
        if file_end != 'No END file selected':
            concatenate_file(newname, file_end, commentchar)
            output.insert(END, "Processing END file...")
        else:
            output.insert(END, "No END file to process...")        
    else:
        if file_vclean != 'No V-CLEAN file selected':
            othername = modify_main(file_vclean, extension_label, comment_label) 
            output.insert(END, "Processing V-CLEAN file...")
        else:
            output.insert(END, "No V-CLEAN file to process...")
        if file_clean != 'No CLEAN file selected':
            lastname = modify_main(file_clean, extension_label, comment_label) 
            output.insert(END, "Processing CLEAN file...")
        else:
            output.insert(END, "No CLEAN file to process...")
    output.insert(END, "File " + newname + " written" + '\n')
    output.insert(END, ".... Files completed...." + '\n')
    return()
# About the application
def About():
    popup = Tk()
    popup.wm_title("ABOUT MODfef.py")
    msg="This application will work with the output files from F-Engrave\nIt will modify the Gcode file to change the comment character\nand change the file extension.\nIt does not overwrite or delete files however, if you create a new file with the same\nname as an existing file it will destroy the existing one without warning.\n\n Author: Ian Jobson\nVersion " + version + "\n"
    label = Label(popup, text=msg)
    label.pack(side="top", fill="x", pady=10, padx=10)
    B1 = Button(popup, text="Dismiss", command = popup.destroy)
    B1.pack()
    popup.mainloop()
# Directions
def Directions():
    popup = Tk()
    popup.wm_title("Directions for MODfef.py")
    msg="First select a MAIN gcode file, this will be the file that you created using F-Engrave\nThen select a VCLEAN file, if you didn't create one in F-Engrave, don't select one\nThen select the CLEAN file, if you didn't create one don't select one\nChoose your file extension, your comment character, and your tool change gcode line.\n Check the box if you want to concatenate the files (one big file)\nNow select start and the files will be created very quickly\n\nYour changes will be saved in the ini file however, the defaults will remain the same unless you choose to overwrite them from the menu\nYou can also load your last setting from the menu."
    label = Label(popup, text=msg)
    label.pack(side="top", fill="x", pady=10, padx=10)
    B1 = Button(popup, text="Dismiss", command = popup.destroy)
    B1.pack()
    popup.mainloop()
# Exit Program
def exit_program():
    window.destroy()
    exit()
#main:
check_configfile()
extension = config_read('extension', 'Default')
commentchar = config_read('commentchar', 'Default')
toolchange = config_read('toolchange', 'Default')
concatenate = config_read('concatenate', 'Default')
window = Tk()
window.title("Modify F-Engrave Output Files Version: " + version)
window.geometry('1250x540')
window.configure(background="lightgray")
# window.resizable(width=False, height=False)
conc_file = BooleanVar(value=concatenate)
# set columns 0, 1 and 2 and rows 0 and 1 to expand to the full possible size
window.columnconfigure(0, weight=1)
window.columnconfigure(1, weight=1)
window.columnconfigure(2, weight=1)
window.rowconfigure(0, weight=1)
window.rowconfigure(1, weight=1)
window.rowconfigure(2, weight=1)
window.rowconfigure(3, weight=1)
window.rowconfigure(4, weight=1)
window.rowconfigure(5, weight=1)
window.rowconfigure(6, weight=1)
window.rowconfigure(7, weight=1)
window.rowconfigure(8, weight=1)
window.rowconfigure(9, weight=1)
window.rowconfigure(10, weight=1)
window.rowconfigure(11, weight=1)
# main file selection label and button
Label (window, text="Click the button to select the MAIN gcode file:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=1, column=0, sticky=W, padx=5)
first_file_button = Button(window, text="Select...", width=9, command=lambda: select_file("first_file")) 
first_file_button.grid(row=1, column=1, sticky='nsew', padx=5, pady=5)
first_file = Label(window, text="No MAIN file selected", bg="lightgray", fg="black", font="none 10 normal")
first_file.grid(row=1, column=2, columnspan=2, sticky='nsew', padx=5, pady=5)
# V-clean file selection label and button
Label (window, text="Click the button to select the V-CLEAN gcode file:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=2, column=0, sticky=W, padx=5)
vclean_button = Button(window, text="Select...", width=9, command=lambda: select_file("vclean_file")) 
vclean_button.grid(row=2, column=1, sticky='nsew', padx=5, pady=5)
vclean_file = Label(window, text="No V-CLEAN file selected", bg="lightgray", fg="black", font="none 10 normal")
vclean_file.grid(row=2, column=2, sticky='nsew', padx=5, pady=5)
# Clean file selection label and button
Label (window, text="Click the button to select the CLEAN gcode file:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=3, column=0, sticky=W, padx=5)
clean_button = Button(window, text="Select...", width=9, command=lambda: select_file("clean_file"))
clean_button.grid(row=3, column=1, sticky='nsew', padx=5, pady=5)
clean_file = Label(window, text="No CLEAN file selected", bg="lightgray", fg="black", font="none 10 normal")
clean_file.grid(row=3, column=2, sticky='nsew', padx=5, pady=5)
# Begin file selection label and button
Label (window, text="Click the button to select the START gcode file:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=4, column=0, sticky=W, padx=5)
begin_file_button = Button(window, text="Select...", width=9, command=lambda: select_file("begin_file")) 
begin_file_button.grid(row=4, column=1, sticky='nsew', padx=5, pady=5)
begin_file = Label(window, text="No START file selected", bg="lightgray", fg="black", font="none 10 normal")
begin_file.grid(row=4, column=2, columnspan=2, sticky='nsew', padx=5, pady=5)
# End file selection label and button
Label (window, text="Click the button to select the END gcode file:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=5, column=0, sticky=W, padx=5)
end_file_button = Button(window, text="Select...", width=9, command=lambda: select_file("end_file")) 
end_file_button.grid(row=5, column=1, sticky='nsew', padx=5, pady=5)
end_file = Label(window, text="No END file selected", bg="lightgray", fg="black", font="none 10 normal")
end_file.grid(row=5, column=2, columnspan=2, sticky='nsew', padx=5, pady=5)
# Extension selection
Label (window, text="The desired Gcode file extension is:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=6, column=0, sticky=W, padx=5)
gyro = Label (window, text=extension, bg="lightgray", fg="black", font="none 10 normal")
gyro.grid(row=6, column=2, sticky='nsew', padx=5, pady=5)
extension_button = Button(master=window, text='Change...',  width=9, command= lambda: user_input('extension')) 
extension_button.grid(row=6, column=1, sticky='nsew', padx=5, pady=5)
# Comment selection
Label (window, text="The desired comment character is:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=7, column=0, sticky='w', padx=5, pady=5)
pizza = Label (window, text=commentchar, bg="lightgray", fg="black", font="none 10 normal") 
pizza.grid(row=7, column=2, sticky='nsew', padx=5, pady=5)
commentchar_button = Button(master=window, text="Change...", width=9, command=lambda: user_input('commentchar')) 
commentchar_button.grid(row=7, column=1, sticky='nsew', padx=5, pady=5)
# Tool change code selection
Label (window, text="The tool change Gcode is:", bg="lightgray", fg="black", font="none 10 normal") .grid(row=8, column=0, sticky=W, padx=5)
cod = Label (window, text=toolchange, bg="lightgray", fg="black", font="none 10 normal")
cod.grid(row=8, column=2, sticky='nsew', padx=5, pady=5)
toolchange_button = Button(master=window, text="Change...", width=9, command=lambda: user_input('toolchange'))
toolchange_button.grid(row=8, column=1, sticky='nsew', padx=5, pady=5)
# The file concatenation select box
checkb = Checkbutton(window, text="Concatonate Files", bg="lightgray", offvalue=0, onvalue=1, variable=conc_file, command=check_box)
checkb.grid(row=9, column=1, sticky='nsew', padx=5, pady=5)
# This label is just sitting above the feedback box
Label (window, text="MESSAGES:", bg="lightgray", fg="black", font="none 10 bold") .grid(row=10, column=0, sticky=W, padx=5)
#create the output text box
scroller = Scrollbar(window)
scroller.grid( row = 10, column = 1, sticky='nsew', padx=5, pady=5)
#output = Text(window, width=75, height=6, wrap=WORD, background="white", yscrollcommand=scroller.set)
output = Listbox(window, width=60, height=16, background="white", yscrollcommand=scroller.set)
output.grid(row=10, column=0, sticky='nsew', padx=5, pady=5)
scroller.config( command = output.yview )
# Instructions Label
#instruction_label = Label(window, text="1. Select the MAIN gcode file  \n  2. Select the VCLEAN gcode file\n3. Select the CLEAN gcode file\n4. Set the gcode file extension\n5. Set the comments character\n   6. Set the gcode for tool change \n7. Choose concatenate option", bd=1, width=40, height=9, anchor=W, relief="solid")
#instruction_label.grid(row=8, column=2, sticky=W)
instruction_frame = Frame(window, bd=5, relief="groove", width=100)
instruction_frame.grid(row=10, column=2, padx=5, pady=5, sticky='nsew')
instruction1_label = Label(instruction_frame, text="1. Select the MAIN gcode file")
instruction1_label.grid(row=1, column=4, sticky=W)
instruction2_label = Label(instruction_frame, text="2. Select the VCLEAN gcode file")
instruction2_label.grid(row=2, column=4, sticky=W)
instruction3_label = Label(instruction_frame, text="3. Select the CLEAN gcode file")
instruction3_label.grid(row=3, column=4, sticky=W)
instruction4_label = Label(instruction_frame, text="4. Set the gcode file extension")
instruction4_label.grid(row=4, column=4, sticky=W)
instruction5_label = Label(instruction_frame, text="5. Set the comments character")
instruction5_label.grid(row=5, column=4, sticky=W)
instruction6_label = Label(instruction_frame, text="6. Set the gcode for the tool change")
instruction6_label.grid(row=6, column=4, sticky=W)
instruction7_label = Label(instruction_frame, text="7. Choose the concatenation option")
instruction7_label.grid(row=7, column=4, sticky=W)
instruction8_label = Label(instruction_frame, text="8. Choose the optional start file")
instruction8_label.grid(row=8, column=4, sticky=W)
instruction9_label = Label(instruction_frame, text="9. Choose the optional end file")
instruction9_label.grid(row=9, column=4, sticky=W)
instruction10_label = Label(instruction_frame, font="none 10 italic", text="The items will go gray as you choose them this is just a reminder of what is selected.")
instruction10_label.grid(row=11, column=4, sticky=W)

# exit button
exit_button = Button(window, text="Exit", width=6, command=exit_program) 
exit_button.grid(row=11, column=0, sticky='nsew', padx=5, pady=5)
# Process the files button
startb = Button(window, text="Start...", width=7, command=start_button) 
startb.grid(row=11, column=1, sticky='nsew', padx=5, pady=5)
# ToolTips for widgets
tip_checkb = ToolTip(checkb, text="If this is checked we will concetenate all the files together")
tip_output = ToolTip(output, text="The system messages will appear here")
tip_cod = ToolTip(cod, text="This is the tool change gcode")
tip_pizza = ToolTip(pizza, text="This is the character that will be used for comments")
tip_gyro = ToolTip(gyro, text="This is the extension that will be used for all written files")
tip_exit_button = ToolTip(exit_button, text="Click here to exit the application")
tip_startb = ToolTip(startb, text="Click here to start processing the files")
tip_toolchange_button = ToolTip(toolchange_button, text="Click here to change the tool change gcode")
tip_commentchar_button = ToolTip(commentchar_button, text="Click here to change the comment character")
tip_extension_button = ToolTip(extension_button, text="Click here to change the extension used for all written files")
tip_first_file_button = ToolTip(first_file_button,"Click here to select the MAIN gcode file")
tip_vclean_button = ToolTip(vclean_button,"Click here to select the VCLEAN gcode file")
tip_clean_button = ToolTip(clean_button,"Click here to select the CLEAN gcode file")
tip_start_button = ToolTip(begin_file_button,"Click here to select the START gcode file")
tip_end_button = ToolTip(end_file_button,"Click here to select the END gcode file")
# Window Menu
menu = Menu(window)
window.config(menu=menu)
# File Menu
filemenu = Menu(menu)
menu.add_cascade(label="File", menu=filemenu)
filemenu.add_command(label="Load User Settings", command=config_read_user)
filemenu.add_command(label="Save Default Settings", command=config_default_save)
filemenu.add_separator()
filemenu.add_command(label="Open MAIN...", command=lambda: select_file("first_file"))
filemenu.add_command(label="Open V-CLEAN...", command=lambda: select_file("vclean_file"))
filemenu.add_command(label="Open CLEAN...", command=lambda: select_file("clean_file"))
filemenu.add_command(label="Open START...", command=lambda: select_file("begin_file"))
filemenu.add_command(label="Open END...", command=lambda: select_file("end_file"))
filemenu.add_separator()
filemenu.add_command(label="Exit", command=exit_program)

# Input menu
input_menu = Menu(menu)
menu.add_cascade(label="Input", menu=input_menu)
input_menu.add_command(label="File extension...", command=lambda:user_input('extension'))
input_menu.add_command(label="Comment Character...", command=lambda:user_input('commentchar'))
input_menu.add_command(label="Tool Change Code...", command=lambda:user_input('toolchange'))
input_menu.add_command(label="Concatonate files...", command=toggle_check)

# Start menu
start_menu = Menu(menu)
menu.add_cascade(label="Start", menu=start_menu)
start_menu.add_command(label="Process Files...", command=start_button)

# Help menu
helpmenu = Menu(menu)
menu.add_cascade(label="Help", menu=helpmenu)
helpmenu.add_command(label="Directions...", command=Directions)
helpmenu.add_command(label="About...", command=About)
window.mainloop()