## README ##

* This application is used to modify the output files from F-engrave vcarve
* Version **2.1.1**
* License: **MIT**
* Owner: **ianj001**


### Set up process ###

* For Windows
    + Download the zip file and extract it where you want the application to run
    + It will install the MODfef.exe file and the MODfef.ini file
* For Linux
    + Install python3
    + Install python-tk
    + git clone ianj001@bitbucket.org/ianj001/modfef.git
    + python MODfef2.py
* Configuration
    + The configuration can be changed and saved from the menu
    + The changes will be stored in the MODfef.ini file
* Dependencies
    + For Linux a working python install
* How to run tests
    + Follow the instruction on how to create the F-engrave output files then process them through the application


### Contribution guidelines ###

* All contribution, modifications and enhancements can be suggested through pull request.


### Who do I talk to? ###

* Repo owner
    + Ian Jobson
* Other community or team contact
    + ianj001 on V1Engineering forums